-- MySQL dump 10.16  Distrib 10.1.25-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: onlinestore
-- ------------------------------------------------------
-- Server version	10.1.25-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categorias`
--

DROP TABLE IF EXISTS `categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias`
--

LOCK TABLES `categorias` WRITE;
/*!40000 ALTER TABLE `categorias` DISABLE KEYS */;
INSERT INTO `categorias` VALUES (1,'VideoJuegos');
/*!40000 ALTER TABLE `categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudades`
--

DROP TABLE IF EXISTS `ciudades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ciudades` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudades`
--

LOCK TABLES `ciudades` WRITE;
/*!40000 ALTER TABLE `ciudades` DISABLE KEYS */;
INSERT INTO `ciudades` VALUES (1,'Bogotà');
/*!40000 ALTER TABLE `ciudades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estados`
--

DROP TABLE IF EXISTS `estados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estados` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(255) NOT NULL,
  `Tipo_estado` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estados`
--

LOCK TABLES `estados` WRITE;
/*!40000 ALTER TABLE `estados` DISABLE KEYS */;
INSERT INTO `estados` VALUES (1,'Estado Productos','Activo');
/*!40000 ALTER TABLE `estados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaproducto`
--

DROP TABLE IF EXISTS `facturaproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaproducto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Cantidad` int(11) NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `Id_factura` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProductoFFk` (`Id_producto`),
  KEY `IdFactura` (`Id_factura`),
  CONSTRAINT `IdFactura` FOREIGN KEY (`Id_factura`) REFERENCES `facturas` (`Id`),
  CONSTRAINT `IdProductoFFk` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaproducto`
--

LOCK TABLES `facturaproducto` WRITE;
/*!40000 ALTER TABLE `facturaproducto` DISABLE KEYS */;
INSERT INTO `facturaproducto` VALUES (1,10,1,2),(2,60,2,3),(3,20,3,3),(4,90,2,3),(5,40,5,1),(6,5,1,4),(7,6,3,4),(8,4,5,4),(9,8,1,5),(10,1,3,5),(11,1,4,5),(12,3,1,6);
/*!40000 ALTER TABLE `facturaproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturas`
--

DROP TABLE IF EXISTS `facturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Fecha` date NOT NULL,
  `Iva` double NOT NULL,
  `Descuento` double NOT NULL,
  `Total` double NOT NULL,
  `Id_persona` int(11) NOT NULL,
  `Estado_pedido` varchar(21) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdPersonaFacturaFk` (`Id_persona`),
  CONSTRAINT `IdPersonaFacturaFk` FOREIGN KEY (`Id_persona`) REFERENCES `personas` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturas`
--

LOCK TABLES `facturas` WRITE;
/*!40000 ALTER TABLE `facturas` DISABLE KEYS */;
INSERT INTO `facturas` VALUES (1,'2020-05-18',0,0,20000,2,'E'),(2,'2020-05-12',0,10,63000,2,'E'),(3,'2020-04-02',0,15,20000,2,'E'),(4,'2020-05-26',0,0,13100000,5,'E'),(5,'2020-05-26',0,0,17000000,6,'E'),(6,'2020-05-26',0,0,6000000,7,'E');
/*!40000 ALTER TABLE `facturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personas`
--

DROP TABLE IF EXISTS `personas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personas` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Documento` int(11) NOT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `Telefono` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Nombres` varchar(255) NOT NULL,
  `Apellidos` varchar(255) NOT NULL,
  `Fecha_registro` date NOT NULL,
  `Id_tipo_documento` int(11) NOT NULL,
  `Id_ciudad` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `TipoDocumentFk` (`Id_tipo_documento`),
  KEY `IdCIaudadFk` (`Id_ciudad`),
  CONSTRAINT `IdCIaudadFk` FOREIGN KEY (`Id_ciudad`) REFERENCES `ciudades` (`Id`),
  CONSTRAINT `TipoDocumentFk` FOREIGN KEY (`Id_tipo_documento`) REFERENCES `tipodocumentos` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personas`
--

LOCK TABLES `personas` WRITE;
/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
INSERT INTO `personas` VALUES (1,1031158874,'Calle 100','3118564047','asd@asd.com','admin','admin','2020-05-17',1,1),(2,99999,'calle 200','7159243','asd@asd.com','nombre cliente','apellido cliente','2020-05-18',1,1),(3,1031158874,'calle 200','123',NULL,'asd','asd','2020-05-26',1,1),(4,1031158874,'calle 200','123',NULL,'asd','asd','2020-05-26',1,1),(5,1031158874,'calle 200','123',NULL,'asd','asd','2020-05-26',1,1),(6,123456789,'calle 200','123456',NULL,'prueba compra','prueba compra','2020-05-26',1,1),(7,987456,'cale 200','123456','asd@asd.com','prueba compra','prueba compra','2020-05-26',1,1);
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `precioproducto`
--

DROP TABLE IF EXISTS `precioproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `precioproducto` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Precio` double NOT NULL,
  `Fecha` date NOT NULL,
  `Id_producto` int(11) NOT NULL,
  `Id_estado` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdProductoFk` (`Id_producto`),
  KEY `IdEstadoPrecioFk` (`Id_estado`),
  CONSTRAINT `IdEstadoPrecioFk` FOREIGN KEY (`Id_estado`) REFERENCES `estados` (`Id`),
  CONSTRAINT `IdProductoFk` FOREIGN KEY (`Id_producto`) REFERENCES `productos` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `precioproducto`
--

LOCK TABLES `precioproducto` WRITE;
/*!40000 ALTER TABLE `precioproducto` DISABLE KEYS */;
INSERT INTO `precioproducto` VALUES (1,2000000,'2020-04-21',1,1),(2,4000000,'2020-04-21',2,1),(3,500000,'2020-05-05',3,1),(4,500000,'2020-05-20',4,1),(5,25000,'2020-05-20',5,1);
/*!40000 ALTER TABLE `precioproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productos`
--

DROP TABLE IF EXISTS `productos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(255) NOT NULL,
  `Cantidad` int(11) NOT NULL,
  `Fecha` date NOT NULL,
  `Id_categoria` int(11) NOT NULL,
  `Url_img` text,
  PRIMARY KEY (`Id`),
  KEY `IdCategoriaFk` (`Id_categoria`),
  CONSTRAINT `IdCategoriaFk` FOREIGN KEY (`Id_categoria`) REFERENCES `categorias` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productos`
--

LOCK TABLES `productos` WRITE;
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` VALUES (1,'Consola Xbox One x',10,'2020-04-12',1,'https://cdn.pocket-lint.com/r/s/970x/assets/images/141329-games-review-xbox-one-x-review-image2-28fwsqx4oi-jpg.webp'),(2,'Consola Play 5',25,'2020-04-12',1,'https://img-cdn.hipertextual.com/files/2020/04/hipertextual-luce-dualsense-playstation-5-otros-colores-2020373639.jpg?strip=all&lossy=1&quality=70&resize=740%2C490&ssl=1'),(3,'PRUEBA DOS',58000,'2020-05-05',1,'https://cdn.pocket-lint.com/r/s/970x/assets/images/141329-games-review-xbox-one-x-review-image2-28fwsqx4oi-jpg.webp'),(4,'PRUEBA DOS',58000,'2020-05-20',1,'https://cdn.pocket-lint.com/r/s/970x/assets/images/141329-games-review-xbox-one-x-review-image2-28fwsqx4oi-jpg.webp'),(5,'Prueba desde front',5,'2020-05-20',1,'https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fgrand-theft-auto-v%2Fhome%2FGTAV_EGS_Artwork_1920x1080_Hero-Carousel_V06-1920x1080-1503e4b1320d5652dd4f57466c8bcb79424b3fc0.jpg?h=1080&resize=1&w=1920');
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Cliente'),(2,'Admin'),(3,'Vendedor');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipodocumentos`
--

DROP TABLE IF EXISTS `tipodocumentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipodocumentos` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Desc` varchar(255) NOT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipodocumentos`
--

LOCK TABLES `tipodocumentos` WRITE;
/*!40000 ALTER TABLE `tipodocumentos` DISABLE KEYS */;
INSERT INTO `tipodocumentos` VALUES (1,'CC'),(2,'CE'),(3,'TI');
/*!40000 ALTER TABLE `tipodocumentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Usuario` varchar(255) NOT NULL,
  `Clave` varchar(255) NOT NULL,
  `Id_persona` int(11) NOT NULL,
  `Id_estado` int(11) NOT NULL,
  `Id_rol` int(11) NOT NULL,
  PRIMARY KEY (`Id`),
  KEY `IdPersonaFk` (`Id_persona`),
  KEY `IdEstadoFk` (`Id_estado`),
  KEY `IdRolFk` (`Id_rol`),
  CONSTRAINT `IdEstadoFk` FOREIGN KEY (`Id_estado`) REFERENCES `estados` (`Id`),
  CONSTRAINT `IdPersonaFk` FOREIGN KEY (`Id_persona`) REFERENCES `personas` (`Id`),
  CONSTRAINT `IdRolFk` FOREIGN KEY (`Id_rol`) REFERENCES `roles` (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'admin','123456789',1,1,2),(2,'cliente','123456789',2,1,1),(3,'1031158874','123456789',3,1,1),(4,'1031158874','123456789',4,1,1),(5,'1031158874','123456789',5,1,1),(6,'123456789','123456789',6,1,1),(7,'987456','123456789',7,1,1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-05-26 15:16:02
