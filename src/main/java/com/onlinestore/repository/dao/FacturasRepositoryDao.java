package com.onlinestore.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinestore.dto.TotalMesDto;
import com.onlinestore.entity.Facturas;

public interface FacturasRepositoryDao extends JpaRepository<Facturas, Integer>{
	
	 @Query(value = "select sum(total) as totalFacturas from facturas", nativeQuery = true)
		 public int totalFacturas ();
	 
	 @Query(value = "select count(*) as pendienteEnvio from facturas where Estado_pedido = 'P'", nativeQuery = true)
	 public int pendienteEnvio ();
	 
	 @Query(value = "SELECT MONTH(Fecha) as name, SUM(total) as value" + 
	 		"      FROM facturas" + 
	 		"	where YEAR(Fecha) = ?1" + 
	 		"      GROUP BY name;", nativeQuery = true)
	 public List<TotalMesDto> totalMes (@Param("anio") String anio);

}
