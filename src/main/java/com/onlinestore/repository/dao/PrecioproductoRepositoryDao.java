package com.onlinestore.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinestore.entity.Precioproducto;

public interface PrecioproductoRepositoryDao extends JpaRepository<Precioproducto, Integer> {

}
