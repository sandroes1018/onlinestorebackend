package com.onlinestore.repository.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.onlinestore.entity.Personas;


public interface PersonasRepositoryDao extends JpaRepository<Personas, Integer> {
	
	 @Query(value = "select count(*) from personas p " + 
	 		"inner join usuarios u on p.Id = u.Id_persona " + 
	 		"where u.Id_rol = 1", nativeQuery = true)
	 public int countClients ();
 
}
