package com.onlinestore.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinestore.dto.ProductosMasVendidosDto;
import com.onlinestore.entity.Facturaproducto;
import com.onlinestore.entity.Facturas;

public interface FacturaProductoRepositoryDao extends JpaRepository<Facturaproducto, Integer>{

	 @Query(value = "select sum(fp.cantidad) as value, p.Nombre as name from facturaproducto fp\r\n" + 
	 		"	inner join productos p on fp.id_producto = p.id\r\n" + 
	 		"GROUP BY fp.id_producto limit 6 ", nativeQuery = true)
		 public List<ProductosMasVendidosDto> productosMasVendidos ();
	 
	 	 public List<Facturaproducto> findByFactura(@Param("factura") Facturas factura);
}
