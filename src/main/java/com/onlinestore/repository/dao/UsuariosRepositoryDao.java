package com.onlinestore.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.onlinestore.entity.Roles;
import com.onlinestore.entity.Usuarios;
@Repository
public interface UsuariosRepositoryDao extends JpaRepository<Usuarios, Integer>{
	
	public Usuarios login(@Param("clave")String clave,@Param("user")String user);
	
	public List<Usuarios> getClients(@Param("idrol")Roles rol);
	
}
