package com.onlinestore.repository.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinestore.entity.Categorias;



public interface CategoriasRepositoryDao extends JpaRepository<Categorias, Integer> {
	List<Categorias> findAll();
}
