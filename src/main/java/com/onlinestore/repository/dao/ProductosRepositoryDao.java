package com.onlinestore.repository.dao;

import java.util.List;

import com.onlinestore.entity.Productos;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductosRepositoryDao extends JpaRepository<Productos, Integer>{
	List<Productos> findAll();
}
