package com.onlinestore.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.service.interfaz.AdminService;
import com.onlinestore.utils.MessageUtilReturn;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private AdminService adminService;

	@RequestMapping(value = "/getdatadashboard",
				method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> loginUser() throws Exception {
		try {
			return new ResponseEntity<>(adminService.getDataDashBoard(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(MessageUtilReturn.responseMessage("Msg", "Ocurrio un error \n"+e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

}
