package com.onlinestore.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.dto.PersonasDto;
import com.onlinestore.dto.ProductosDto;
import com.onlinestore.service.interfaz.PersonasService;

@RestController
@RequestMapping("/users")
public class UsersController {
	
	@Autowired
	private PersonasService personasService;
	
	public UsersController() {}
	
   	@ResponseBody
    @RequestMapping(value = "/addUsers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?>  addProducto(@RequestBody(required = true) @Valid PersonasDto p) {
    	 	    try {
    	 	    	 return new ResponseEntity<>("", HttpStatus.OK);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e.getMessage());
					 return new ResponseEntity<>("Ocurrio un error", HttpStatus.INTERNAL_SERVER_ERROR);
				}	       
    }
   	
 	@ResponseBody
    @RequestMapping(value = "/getClients", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?>  getClients() {
    	 	    try {
    	 	    	 return new ResponseEntity<>(this.personasService.getClients(), HttpStatus.OK);
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println(e.getMessage());
					 return new ResponseEntity<>("Ocurrio un error", HttpStatus.INTERNAL_SERVER_ERROR);
				}	       
    }
   	
   	

}
