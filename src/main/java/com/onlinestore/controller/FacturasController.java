package com.onlinestore.controller;

import javax.validation.Valid;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.dto.FinCompraDto;
import com.onlinestore.dto.PersonasDto;
import com.onlinestore.dto.ProductoDTO;
import com.onlinestore.service.interfaz.FacturasService;
import com.onlinestore.utils.MessageUtilReturn;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/facturas")
public class FacturasController {

	@Autowired
	private FacturasService facturasService;

	@RequestMapping(value = "/getFacturas", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getFacturas() throws Exception {
		try {
			return new ResponseEntity<>(this.facturasService.getAllFacturas(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(
					MessageUtilReturn.responseMessage("Msg", "Ocurrio un error \n" + e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/getFacturasPending", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> getFacturasPending() throws Exception {
		try {
			return new ResponseEntity<>(this.facturasService.getFacturasPending(), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(
					MessageUtilReturn.responseMessage("Msg", "Ocurrio un error \n" + e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/changeStatus/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarPreciosConsultaML(@PathVariable(value = "id", required = true) int id) {

		try {
			return new ResponseEntity<>(this.facturasService.changeStatus(id), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(
					MessageUtilReturn.responseMessage("Msg", "Ocurrio un error \n" + e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/finVenta", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> guardarPreciosConsultaML(@RequestBody(required = true)FinCompraDto fc) {

		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(fc);
			System.out.println("JSON******************\n"+json);
			return new ResponseEntity<>(this.facturasService.finVenta(fc), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>(
					MessageUtilReturn.responseMessage("Msg", "Ocurrio un error \n" + e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
