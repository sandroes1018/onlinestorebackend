package com.onlinestore.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.dto.UsuarioDto;
import com.onlinestore.service.interfaz.LoginService;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/userPerson")
public class LoginPersonController {
	 @Autowired
	    private LoginService loginService;
	 
	  
	   @RequestMapping(value = "/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<?>  loginUser(@RequestBody UsuarioDto user) {
	    	     	  
		   try {
			   return new ResponseEntity<>(loginService.loginPerson(user), HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<>("Login faild", HttpStatus.UNAUTHORIZED);
		}
	        
	    }
	
}
