package com.onlinestore.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.dto.ProductoDTO;
import com.onlinestore.dto.ProductosDto;
import com.onlinestore.service.interfaz.ProductosService;
import com.onlinestore.utils.MessageUtilReturn;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/productos")
public class ProductosController {

	@Autowired
	private ProductosService productosService;
	
	public ProductosController() {}
	
	   
	   @RequestMapping(value = "/getAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<?>  getAll() {
	    	 	    try {
	    	 	    	 return new ResponseEntity<>(this.productosService.getAllProductos(), HttpStatus.OK);
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e.getMessage());
						 return new ResponseEntity<>("Ocurrio un error", HttpStatus.INTERNAL_SERVER_ERROR);
					}	       
	    }
	   @GetMapping("/getAllCategorias")
	    public ResponseEntity<?>  getAllCategorias() {
	    	 	    try {
	    	 	    	 return new ResponseEntity<>(this.productosService.getAllCategorias(), HttpStatus.OK);
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e.getMessage());
						 return new ResponseEntity<>("Ocurrio un error", HttpStatus.INTERNAL_SERVER_ERROR);
					}	       
	    }

	   	@ResponseBody
	    @RequestMapping(value = "/addProducto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    public ResponseEntity<?>  addProducto(@RequestBody(required = true) @Valid ProductosDto producto) {
	    	 	    try {
	    	 	    	 return new ResponseEntity<>(this.productosService.addProducto(producto), HttpStatus.OK);
					} catch (Exception e) {
						// TODO: handle exception
						System.out.println(e.getMessage());
						 return new ResponseEntity<>("Ocurrio un error", HttpStatus.INTERNAL_SERVER_ERROR);
					}	       
	    }
}
