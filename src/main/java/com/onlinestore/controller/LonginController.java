/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.controller;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.onlinestore.dto.UsuarioDto;
import com.onlinestore.service.interfaz.LoginService;
import com.onlinestore.utils.MessageUtilReturn;

/**
 *
 * @author jlagosmu
 */
@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RequestMapping("/user")
public class LonginController {

    @Autowired
    private LoginService loginService;

    public LonginController() {
    }

//    @GetMapping("/login/token")
//    public UsuarioDto loginUser()  throws Exception{
//       return usuarioDto;
//    }
    @PostMapping("/login")
    public ResponseEntity<?>  loginUser(@RequestBody UsuarioDto user) throws Exception {
    	     	    
        return new ResponseEntity<>(loginService.loginUser(user), HttpStatus.OK);
    }
    @GetMapping("/login/test")
    public ResponseEntity<?>  loginUser() {    	 	    
        return new ResponseEntity<>(MessageUtilReturn.responseMessage("Test", "Sucessfull"), HttpStatus.OK);
    }

}
