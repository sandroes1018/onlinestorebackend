package com.onlinestore.controller;

import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.onlinestore.dto.ProductoDTO;


@RestController
public class HelloController {

    @ResponseBody
    @RequestMapping(value = "/mercadoLibre/{paisML}/producto/compararPrecios", params = {
            "categoria" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getPreciosConsultaML(@PathVariable(value = "paisML", required = true) String paisML,
            @RequestParam(value = "categoria", required = false) String categoria) {
        
        Map<String,String> respuesta= new HashMap<>();
        
        switch (paisML) {
        case "MCO":
            respuesta.put("paisMLIngresado", paisML);
            break;
        case "MCA":
            respuesta.put("paisMLIngresado", paisML);
            break;

        default:
            return new ResponseEntity<>("paisMLIngresado", HttpStatus.BAD_REQUEST);
        }
      
        respuesta.put("paisMLIngresado", paisML);
        respuesta.put("categoriaIngresado", categoria);
        return new ResponseEntity<>(respuesta, HttpStatus.OK);

    }
    
    
    
    @ResponseBody
    @RequestMapping(value = "/mercadoLibre/{paisML}/producto", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> guardarPreciosConsultaML(@RequestBody(required = true) @Valid ProductoDTO producto, @PathVariable(value = "paisML", required = true) String paisML) {
      
        return new ResponseEntity<>(producto, HttpStatus.OK);

    }
    
    @ResponseBody
    @RequestMapping(value = "/getProducts", method = RequestMethod.GET)
    public ResponseEntity<?> getProducts() {
    	
    	try {
    		
    		System.out.println("SERVI PRODUCTS");
    		
    		
    		
    		System.out.println("Tamaño Product Final ");
    		
      } catch (Exception e) {
         
        System.out.println(e.getMessage());
      }
        return new ResponseEntity<>("", HttpStatus.OK);

    }
    
    

    // @ResponseBody
    // @RequestMapping(value =
    // "/users/learning-coaches/{identityId_lc}/students/{identityId_stu}/courses/progress", params = {
    // "stSchoolId", "lcSchoolId" }, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    // public ResponseEntity<?> getStudentCourses(
    // @PathVariable(value = "identityId_lc", required = true) Integer identityIdLC,
    // @PathVariable(value = "identityId_stu", required = true) Integer identityIdSTU,
    // @RequestParam(value = "stSchoolId") Integer schoolIdSTU,
    // @RequestParam(value = "lcSchoolId") Integer schoolIdLC) {
    // StudentCourseResponse response = new StudentCourseResponse();
    // HttpStatus httpStatus = HttpStatus.OK;
    //
    // try {
    // response.setData(studentProgressService.getBaseStudentCourses(identityIdLC, schoolIdLC, identityIdSTU,
    // schoolIdSTU));
    // response.setSuccess(true);
    // response.setMessage("");
    // response.setResponseCode(200);
    //
    // } catch (Exception e) {
    // LOGGER.error("Exception getting courses", e);
    // response.setSuccess(false);
    // response.setMessage(e.getMessage());
    // response.setResponseCode(500);
    // httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
    // }
    // return new ResponseEntity<>(response, httpStatus);
    // }

}
