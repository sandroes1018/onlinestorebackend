/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ENVY 15
 */
@Entity
@Table(name = "precioproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Precioproducto.findAll", query = "SELECT p FROM Precioproducto p"),
    @NamedQuery(name = "Precioproducto.findById", query = "SELECT p FROM Precioproducto p WHERE p.id = :id"),
    @NamedQuery(name = "Precioproducto.findByPrecio", query = "SELECT p FROM Precioproducto p WHERE p.precio = :precio"),
    @NamedQuery(name = "Precioproducto.findByFecha", query = "SELECT p FROM Precioproducto p WHERE p.fecha = :fecha")})
public class Precioproducto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Precio")
    private double precio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "Id_estado", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Estados idestado;
    @JoinColumn(name = "Id_producto", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Productos idproducto;

    public Precioproducto() {
    }

    public Precioproducto(Integer id) {
        this.id = id;
    }

    public Precioproducto(Integer id, double precio, Date fecha) {
        this.id = id;
        this.precio = precio;
        this.fecha = fecha;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Estados getIdestado() {
        return idestado;
    }

    public void setIdestado(Estados idestado) {
        this.idestado = idestado;
    }

    public Productos getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Productos idproducto) {
        this.idproducto = idproducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Precioproducto)) {
            return false;
        }
        Precioproducto other = (Precioproducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onlinestore.entity.Precioproducto[ id=" + id + " ]";
    }
    
}
