/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ENVY 15
 */
@Entity
@Table(name = "facturaproducto")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Facturaproducto.findAll", query = "SELECT f FROM Facturaproducto f"),
    @NamedQuery(name = "Facturaproducto.findById", query = "SELECT f FROM Facturaproducto f WHERE f.id = :id"),
    @NamedQuery(name = "Facturaproducto.findByCantidad", query = "SELECT f FROM Facturaproducto f WHERE f.cantidad = :cantidad"),
    @NamedQuery(name = "Facturaproducto.findByFactura", query = "SELECT f FROM Facturaproducto f WHERE f.idfactura = :factura")
    })
public class Facturaproducto implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Cantidad")
    private int cantidad;
    @JoinColumn(name = "Id_factura", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Facturas idfactura;
    @JoinColumn(name = "Id_producto", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Productos idproducto;

    public Facturaproducto() {
    }

    public Facturaproducto(Integer id) {
        this.id = id;
    }

    public Facturaproducto(Integer id, int cantidad) {
        this.id = id;
        this.cantidad = cantidad;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Facturas getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(Facturas idfactura) {
        this.idfactura = idfactura;
    }

    public Productos getIdproducto() {
        return idproducto;
    }

    public void setIdproducto(Productos idproducto) {
        this.idproducto = idproducto;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Facturaproducto)) {
            return false;
        }
        Facturaproducto other = (Facturaproducto) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.onlinestore.entity.Facturaproducto[ id=" + id + " ]";
    }
    
}
