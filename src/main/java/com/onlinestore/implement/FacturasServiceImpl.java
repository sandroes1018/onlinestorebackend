package com.onlinestore.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.dto.FacturasDto;
import com.onlinestore.dto.FinCompraDto;
import com.onlinestore.dto.ProductosCompradosDto;
import com.onlinestore.dto.ProductosDto;
import com.onlinestore.entity.Facturaproducto;
import com.onlinestore.entity.Facturas;
import com.onlinestore.entity.Personas;
import com.onlinestore.entity.Productos;
import com.onlinestore.repository.dao.FacturaProductoRepositoryDao;
import com.onlinestore.repository.dao.FacturasRepositoryDao;
import com.onlinestore.service.interfaz.FacturasService;
import com.onlinestore.service.interfaz.PersonasService;
import com.onlinestore.utils.MessageUtilReturn;

@Service
public class FacturasServiceImpl implements FacturasService {
	
	private Facturas facturas;
	
	@Autowired
	private FacturasRepositoryDao facturasRepositoryDao;
	@Autowired
	private FacturaProductoRepositoryDao facturaProductoRepositoryDao;
	@Autowired
	private PersonasService personasService;
	
	@Override
	public List<FacturasDto> getAllFacturas(){
		
		List<FacturasDto> listDto = new ArrayList<FacturasDto>();
		
		
		List<Facturas> list = this.facturasRepositoryDao.findAll();
		
		
		
		
		
		for (Facturas facturas : list) {
			
			FacturasDto tmp = new FacturasDto(facturas);			
			List<Facturaproducto> listTmp = this.facturaProductoRepositoryDao.findByFactura(facturas);		
			List<ProductosDto> listProduct = new ArrayList<ProductosDto>();
			listTmp.forEach((Facturaproducto fp) -> listProduct.add(new ProductosDto(fp.getIdproducto())));			
			tmp.setListProducts(listProduct);	
			
			listDto.add(tmp);
			
		}
		
		return listDto;
		
	}
	
	@Override
	public List<FacturasDto> getFacturasPending(){
		
		List<FacturasDto> listDto = new ArrayList<FacturasDto>();		
		List<Facturas> list = this.facturasRepositoryDao.findAll();
	
		for (Facturas facturas : list) {
			
			if(facturas.getEstadopedido().equals("P")) {
				FacturasDto tmp = new FacturasDto(facturas);			
				List<Facturaproducto> listTmp = this.facturaProductoRepositoryDao.findByFactura(facturas);		
				List<ProductosDto> listProduct = new ArrayList<ProductosDto>();
				listTmp.forEach((Facturaproducto fp) -> listProduct.add(new ProductosDto(fp.getIdproducto())));			
				tmp.setListProducts(listProduct);
				listDto.add(tmp);
			}	
		}
		
		return listDto;
		
	}

	@Override
	public HashMap changeStatus(int id) {
		// TODO Auto-generated method stub
		Facturas f = this.facturasRepositoryDao.findById(id).get();
		
		f.setEstadopedido("E");
		this.facturasRepositoryDao.save(f);
		
		return MessageUtilReturn.responseMessage("Msg", "Cambio exitoso");
	}

	@Override
	public HashMap finVenta(FinCompraDto fc) throws Exception {
		// TODO Auto-generated method stub
		Personas p = this.personasService.addUsers(fc.getComprador());
		
		this.facturas = new Facturas();
		this.facturas.setDescuento(0);
		this.facturas.setEstadopedido("P");
		this.facturas.setFecha(new Date());
		this.facturas.setIdpersona(new Personas(p.getId()));
		this.facturas.setIva(0);
		
		double totalFactura = 0;
		
		for (ProductosCompradosDto pc : fc.getProductos()) {
			totalFactura = totalFactura + pc.getCantidadIngresada() * pc.getProdcuto().getPrecio();
		}
		this.facturas.setTotal(totalFactura);
		
		this.facturasRepositoryDao.save(this.facturas);
		
		for (ProductosCompradosDto pc : fc.getProductos()) {
			
			Facturaproducto facturaproducto = new Facturaproducto();
			
			facturaproducto.setCantidad(pc.getCantidadIngresada());
			facturaproducto.setIdfactura(this.facturas);
			facturaproducto.setIdproducto(new Productos(pc.getProdcuto().getId()));
			
			this.facturaProductoRepositoryDao.save(facturaproducto);
		}
		
		return MessageUtilReturn.responseMessage("Msg", "Factura creada exitosamente");
	}
	
	
}
