package com.onlinestore.implement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.dto.PersonasDto;
import com.onlinestore.dto.ProductosDto;
import com.onlinestore.entity.Ciudades;
import com.onlinestore.entity.Estados;
import com.onlinestore.entity.Personas;
import com.onlinestore.entity.Productos;
import com.onlinestore.entity.Roles;
import com.onlinestore.entity.Tipodocumentos;
import com.onlinestore.entity.Usuarios;
import com.onlinestore.repository.dao.PersonasRepositoryDao;
import com.onlinestore.repository.dao.UsuariosRepositoryDao;
import com.onlinestore.service.interfaz.PersonasService;
import com.onlinestore.utils.MessageUtilReturn;
@Service
public class PersonasServiceImpl implements PersonasService {
	
	
	private ObjectMapper mapper;
	private Personas persona;
	private Usuarios usuario;
	
	
	@Autowired
	private PersonasRepositoryDao personasRepositoryDao;
	@Autowired
	private UsuariosRepositoryDao usuariosRepositoryDao;
	
	public PersonasServiceImpl() {
		this.mapper = new ObjectMapper();
	}
	
	
	@Override
	public Personas addUsers(PersonasDto p) throws Exception {
		
		
		try {
			System.out.println("JSON******************\n"+this.mapper.writeValueAsString(p));
			ConvertToDto(p);
			
			this.personasRepositoryDao.save(this.persona);
			converToDtoUsuario(p);
			this.usuariosRepositoryDao.save(this.usuario);
		
			
			return this.persona;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			throw new Exception("Error al insertar el producto");
		} 
		
	}
	
	@Override
	public List<PersonasDto> getClients() throws Exception {
		// TODO Auto-generated method stub
		List<PersonasDto> listDto = new ArrayList<PersonasDto>();
		
		List<Usuarios> list = this.usuariosRepositoryDao.getClients(new Roles(1));
		
		
			list.forEach((Usuarios u) -> listDto.add(new PersonasDto(u.getIdpersona())));
		
		//list.forEach((Productos p) -> dtos.add(new ProductosDto(p)));
		
		return listDto;
	}
	private void converToDtoUsuario(PersonasDto p) {
		this.usuario = new Usuarios();
		
		this.usuario.setClave(p.getClave());
		this.usuario.setUsuario(p.getUsuario1());
		this.usuario.setIdestado(new Estados(1));
		this.usuario.setIdpersona(this.persona);
		this.usuario.setIdrol(new Roles(1));
	}
	private void ConvertToDto(PersonasDto p) {
		this.persona = new Personas();
		
		this.persona.setNombres(p.getNombres());
		this.persona.setApellidos(p.getApellidos());
		this.persona.setIdciudad(new Ciudades(Integer.parseInt(p.getCiudad())));
		this.persona.setFecharegistro(new Date());
		this.persona.setDireccion(p.getDireccion());
		this.persona.setEmail(p.getEmail());
		this.persona.setDocumento(Integer.parseInt( p.getDocumento()));
		this.persona.setIdtipodocumento(new Tipodocumentos(Integer.parseInt(p.getTipo_documento())));
		this.persona.setTelefono(p.getTelefono());
		
		
	}
	
	


	

}
