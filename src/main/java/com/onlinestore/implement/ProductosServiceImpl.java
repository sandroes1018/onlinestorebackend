package com.onlinestore.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.dto.CategoriaDto;
import com.onlinestore.dto.ProductosDto;
import com.onlinestore.entity.Categorias;
import com.onlinestore.entity.Estados;
import com.onlinestore.entity.Precioproducto;
import com.onlinestore.entity.Productos;
import com.onlinestore.repository.dao.CategoriasRepositoryDao;
import com.onlinestore.repository.dao.PrecioproductoRepositoryDao;
import com.onlinestore.repository.dao.ProductosRepositoryDao;
import com.onlinestore.service.interfaz.ProductosService;
import com.onlinestore.utils.MessageUtilReturn;

@Service
public class ProductosServiceImpl implements ProductosService {
	
	private Productos productos;
	@Autowired
	private ProductosRepositoryDao productosRepositoryDao;
	@Autowired
	private CategoriasRepositoryDao categoriaRepositoryDao;
	@Autowired
	private PrecioproductoRepositoryDao precioproductoRepositoryDao;
	
	private ObjectMapper mapper;
	
	public ProductosServiceImpl() {
		this.mapper = new ObjectMapper();
	}

	@Override
	public List<ProductosDto> getAllProductos() {
		// TODO Auto-generated method stub
		List<ProductosDto> dtos = new ArrayList<>();
		List<Productos> list = productosRepositoryDao.findAll();
		list.forEach((Productos p) -> dtos.add(new ProductosDto(p)));
		return dtos;
	}

	@Override
	public HashMap addProducto(ProductosDto p) throws Exception {
		// TODO Auto-generated method stub
		
		try {
			String json = this.mapper.writeValueAsString(p);
			System.out.println("JSON******************\n"+json);
			dtoToEntity(p);	
			productosRepositoryDao.save(this.productos);
			System.out.println("LAST ID PORDUCTO "+this.productos.getId());
			Precioproducto precioproducto = new Precioproducto();		
			precioproducto.setPrecio(p.getPrecio());
			precioproducto.setFecha(new Date());
			precioproducto.setIdproducto(this.productos);
			precioproducto.setIdestado(new Estados(1));
			precioproductoRepositoryDao.save(precioproducto);

			return MessageUtilReturn.responseMessage("Msg","Producto creado correctamente");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			throw new Exception("Error al insertar el producto");
			
		}	
	}

	@Override
	public List<CategoriaDto> getAllCategorias() {
		// TODO Auto-generated method stub
		List<CategoriaDto> dtos = new ArrayList<>();
		List<Categorias> list = categoriaRepositoryDao.findAll();
		list.forEach((Categorias c) -> dtos.add(new CategoriaDto(c)));
		return dtos;
	}
	
	private void dtoToEntity (ProductosDto p) {
		
		this.productos = new Productos();
		this.productos.setCantidad(p.getCantidad());
		this.productos.setNombre(p.getNombre());
		this.productos.setFecha(new Date());
		this.productos.setUrlimg(p.getUrlImg());
		this.productos.setIdcategoria(new Categorias(p.getCategoria().getId()));		
		
	}

}
