package com.onlinestore.implement;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.dto.DashBoardDto;
import com.onlinestore.repository.dao.FacturaProductoRepositoryDao;
import com.onlinestore.repository.dao.FacturasRepositoryDao;
import com.onlinestore.repository.dao.PersonasRepositoryDao;
import com.onlinestore.service.interfaz.AdminService;


@Service
public class AdminServiceImpl implements AdminService {
	
	@Autowired
	private PersonasRepositoryDao personasRepositoryDao;
	@Autowired
	private FacturasRepositoryDao facturasRepositoryDao;
	@Autowired
	private FacturaProductoRepositoryDao facturaProductoRepositoryDao;

	@Override
	public DashBoardDto getDataDashBoard() throws Exception {
		// TODO Auto-generated method stub
		DashBoardDto dashBoard = new DashBoardDto();
			
		dashBoard.setCountClients(personasRepositoryDao.countClients());
		dashBoard.setTotalFacturas(facturasRepositoryDao.totalFacturas());
		dashBoard.setPendienteEnvio(facturasRepositoryDao.pendienteEnvio());
		
		dashBoard.setListMasVendido(facturaProductoRepositoryDao.productosMasVendidos());		
		Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());  //use java.util.Date object as arguement
        // get the value of all the calendar date fields.
        System.out.println("Calendar's Year: " + cal.get(Calendar.YEAR));       
		dashBoard.setListTotalMes(facturasRepositoryDao.totalMes(Integer.toString(cal.get(Calendar.YEAR))));
		
		
		
		return dashBoard;
	}

}
