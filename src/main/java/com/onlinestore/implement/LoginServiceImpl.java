package com.onlinestore.implement;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.onlinestore.dto.PersonasDto;
import com.onlinestore.dto.UsuarioDto;
import com.onlinestore.entity.Usuarios;
import com.onlinestore.repository.dao.UsuariosRepositoryDao;
import com.onlinestore.service.interfaz.LoginService;
import com.onlinestore.utils.Constantes;

import org.jboss.logging.Logger;

@Service
public class LoginServiceImpl implements LoginService{
	
	 
	 @Autowired
	 private UsuariosRepositoryDao usuariosRepositoryDao;
	 
	 @Override
	  public HashMap loginUser(UsuarioDto user) throws Exception {
	    	
	        boolean validacion = false;
	        try {
	            HashMap<String, String> userLogin = new HashMap<String, String>();
	            System.out.println(user.getNombre() +" "+ user.getPassword());
	            if(user.getNombre().equals(Constantes.USER_PUBLIC) && user.getPassword().equals(Constantes.PASS_PUBLIC)) {
	            	validacion = true;
	            	userLogin.put("usuario", user.getNombre());
	            	System.out.println("Login successfull");
	            	return userLogin;
	            }else {
	            	System.out.println("Login fail");
	            	throw new Exception(Constantes.ERROR_LOGIN);
	            }
	           
	                	            
	        } catch (Exception e) {
	        
	        	   if (e.getMessage().contains(Constantes.ERROR_LOGIN)) {
	                   Logger.getLogger(LoginServiceImpl.class.getName())
	                           .log(Logger.Level.ERROR, e);
	                   throw new Exception(Constantes.ERROR_LOGIN);
	               } else {
	                   Logger.getLogger(LoginServiceImpl.class.getName())
	                           .log(Logger.Level.ERROR, e);
	                   throw new Exception("Error inesperado");
	               }
	            }
	    }
	 
	 @Override
	 public PersonasDto loginPerson(UsuarioDto userLogin) throws Exception{
		 		 		 
		 Usuarios user = this.usuariosRepositoryDao.login( userLogin.getPassword(),userLogin.getNombre());
		 
		 if(user != null) {
			 return new PersonasDto(user);
		 }else{
			 throw new Exception(Constantes.ERROR_LOGIN);
		 }		 
	 }

}
