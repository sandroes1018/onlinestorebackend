package com.onlinestore.service.interfaz;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.onlinestore.dto.CategoriaDto;
import com.onlinestore.dto.ProductosDto;

@Service
public interface ProductosService {
	List<ProductosDto> getAllProductos();
	List<CategoriaDto> getAllCategorias();
	HashMap addProducto(ProductosDto p) throws Exception;
}
