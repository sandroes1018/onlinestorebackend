package com.onlinestore.service.interfaz;

import org.springframework.stereotype.Service;

import com.onlinestore.dto.DashBoardDto;

@Service
public interface AdminService {
	public DashBoardDto getDataDashBoard()throws Exception;
}
