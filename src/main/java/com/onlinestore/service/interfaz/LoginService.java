package com.onlinestore.service.interfaz;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import com.onlinestore.dto.PersonasDto;
import com.onlinestore.dto.UsuarioDto;

@Service
public interface LoginService {
	
    public HashMap loginUser(UsuarioDto user) throws Exception;
    
    public PersonasDto loginPerson(UsuarioDto user) throws Exception;
    
}
