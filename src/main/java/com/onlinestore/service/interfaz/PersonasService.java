package com.onlinestore.service.interfaz;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.onlinestore.dto.PersonasDto;
import com.onlinestore.entity.Personas;

@Service
public interface PersonasService {

	Personas addUsers(PersonasDto p) throws Exception;
	
	List<PersonasDto> getClients() throws Exception;
	
}
