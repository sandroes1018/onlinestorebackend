package com.onlinestore.service.interfaz;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.onlinestore.dto.FacturasDto;
import com.onlinestore.dto.FinCompraDto;

@Service
public interface FacturasService {
	public List<FacturasDto> getAllFacturas();
	public List<FacturasDto> getFacturasPending();	
	public HashMap changeStatus(int id);
	public HashMap finVenta(FinCompraDto fc)throws Exception;
	
}
