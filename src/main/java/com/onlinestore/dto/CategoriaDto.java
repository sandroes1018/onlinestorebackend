package com.onlinestore.dto;

import java.io.Serializable;



import com.onlinestore.entity.Categorias;

public class CategoriaDto implements Serializable  {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1160346360740393918L;

	private int Id;
	private String Desc;
	
	public CategoriaDto(Categorias categoria) {
		this.Id = categoria.getId();
		this.Desc = categoria.getDesc();
	}
	
	public CategoriaDto() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getDesc() {
		return Desc;
	}
	public void setDesc(String desc) {
		Desc = desc;
	}
	
	

}
