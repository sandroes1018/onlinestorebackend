package com.onlinestore.dto;

public class ProductosCompradosDto {
	private ProductosDto prodcuto;
	private int cantidadIngresada;
	

	public ProductosDto getProdcuto() {
		return prodcuto;
	}
	public void setProdcuto(ProductosDto prodcuto) {
		this.prodcuto = prodcuto;
	}
	public int getCantidadIngresada() {
		return cantidadIngresada;
	}
	public void setCantidadIngresada(int cantidadIngresada) {
		this.cantidadIngresada = cantidadIngresada;
	}
	
	
}
