package com.onlinestore.dto;

import com.onlinestore.entity.Facturaproducto;

public class FacturaProductosDto {
	 
	    private Integer id;	   
	    private int cantidad;	  
	    private FacturasDto idfactura;	 
	    private ProductosDto idproducto;
	    
	    public FacturaProductosDto(Facturaproducto fp) {
	    	this.idproducto = new ProductosDto(fp.getIdproducto());
	    	this.id = fp.getId();
	    	this.cantidad = fp.getCantidad();
	    }
	    
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public int getCantidad() {
			return cantidad;
		}
		public void setCantidad(int cantidad) {
			this.cantidad = cantidad;
		}
		public FacturasDto getIdfactura() {
			return idfactura;
		}
		public void setIdfactura(FacturasDto idfactura) {
			this.idfactura = idfactura;
		}

		public ProductosDto getIdproducto() {
			return idproducto;
		}

		public void setIdproducto(ProductosDto idproducto) {
			this.idproducto = idproducto;
		}
	
	    
}
