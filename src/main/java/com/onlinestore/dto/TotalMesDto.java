package com.onlinestore.dto;

public interface TotalMesDto {
		
	public String getName();	
	public double getValue();

	
}
