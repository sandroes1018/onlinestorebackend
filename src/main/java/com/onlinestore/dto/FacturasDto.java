package com.onlinestore.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.onlinestore.entity.Facturaproducto;
import com.onlinestore.entity.Facturas;
import com.onlinestore.entity.Personas;

public class FacturasDto {
	
    private Integer id;
 
    private Date fecha;
  
    private double iva;
   
    private double descuento;
   
    private double total;
   
    private PersonasDto idpersona;
    
    private List<FacturaProductosDto> facturaproductoList;
    
    private List<ProductosDto> listProducts;
    
    private String estadopedido;
    
    public FacturasDto(Facturas f) {
    	this.id = f.getId();
    	this.fecha = f.getFecha();
    	this.iva = f.getIva();
    	this.descuento = f.getDescuento();
    	this.total = f.getTotal();
    	this.idpersona = new PersonasDto(f.getIdpersona());    	    
    	this.estadopedido = f.getEstadopedido();
    }

    
	public String getEstadopedido() {
		return estadopedido;
	}


	public void setEstadopedido(String estadopedido) {
		this.estadopedido = estadopedido;
	}


	public List<ProductosDto> getListProducts() {
		return listProducts;
	}


	public void setListProducts(List<ProductosDto> listProducts) {
		this.listProducts = listProducts;
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public double getIva() {
		return iva;
	}

	public void setIva(double iva) {
		this.iva = iva;
	}

	public double getDescuento() {
		return descuento;
	}

	public void setDescuento(double descuento) {
		this.descuento = descuento;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
	

	public PersonasDto getIdpersona() {
		return idpersona;
	}

	public void setIdpersona(PersonasDto idpersona) {
		this.idpersona = idpersona;
	}

	public List<FacturaProductosDto> getFacturaproductoList() {
		return facturaproductoList;
	}

	public void setFacturaproductoList(List<FacturaProductosDto> facturaproductoList) {
		this.facturaproductoList = facturaproductoList;
	}
    
    

}
