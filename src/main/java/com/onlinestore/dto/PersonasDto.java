package com.onlinestore.dto;

import java.io.Serializable;

import com.onlinestore.entity.Personas;
import com.onlinestore.entity.Usuarios;

public class PersonasDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2074638292139187427L;
	private int id;
	private String documento;
	private TipoDocumentosDto tipoDocumento;	
	private String direccion;
	private String telefono;
	private String email;
	private String nombres;
	private String apellidos;
	private String ciudad;
	private String clave;
	private String usuario1;
	private String tipo_documento;
	private UsuarioDto usuario;
	private CiudadesDto ciudadesDto;
	
	
	public PersonasDto() {
	this.usuario = new UsuarioDto();	
	this.tipoDocumento = new TipoDocumentosDto();
	this.ciudadesDto = new CiudadesDto();
	}
	
	public PersonasDto(Usuarios u) {
		
		this.usuario = new UsuarioDto();	
		this.usuario.setNombre(u.getUsuario());
		this.usuario.setRol(u.getIdrol().getDesc());
		this.nombres = u.getIdpersona().getNombres();
		this.id = u.getIdpersona().getId();
		this.tipoDocumento = new TipoDocumentosDto();
		this.ciudadesDto = new CiudadesDto();
		}
	
	public PersonasDto(Personas p) {
		
		this.id = p.getId();
		this.apellidos = p.getApellidos();
		this.nombres = p.getNombres();
		this.documento = Integer.toString(p.getDocumento());
		this.telefono = p.getTelefono();
		this.email = p.getEmail();		
	
		}

	

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getUsuario1() {
		return usuario1;
	}

	public void setUsuario1(String usuario1) {
		this.usuario1 = usuario1;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public CiudadesDto getCiudadesDto() {
		return ciudadesDto;
	}



	public void setCiudadesDto(CiudadesDto ciudadesDto) {
		this.ciudadesDto = ciudadesDto;
	}


	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public TipoDocumentosDto getTipoDocumento() {
		return tipoDocumento;
	}


	public void setTipoDocumento(TipoDocumentosDto tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}


	public String getDireccion() {
		return direccion;
	}


	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}


	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNombres() {
		return nombres;
	}


	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getCiudad() {
		return ciudad;
	}


	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}


	public UsuarioDto getUsuario() {
		return usuario;
	}


	public void setUsuario(UsuarioDto usuario) {
		this.usuario = usuario;
	}
	
		
}
