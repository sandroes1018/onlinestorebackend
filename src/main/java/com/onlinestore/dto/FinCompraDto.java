package com.onlinestore.dto;

import java.util.List;

public class FinCompraDto {
	private List<ProductosCompradosDto> productos;
	private PersonasDto comprador;

	
	public List<ProductosCompradosDto> getProductos() {
		return productos;
	}
	public void setProductos(List<ProductosCompradosDto> productos) {
		this.productos = productos;
	}
	public PersonasDto getComprador() {
		return comprador;
	}
	public void setComprador(PersonasDto comprador) {
		this.comprador = comprador;
	}
	
	
}
