package com.onlinestore.dto;

import java.io.Serializable;

import com.onlinestore.entity.Productos;

public class ProductoDTO implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6383353455591380896L;

    Integer idProducto;
    Float precio;
    Boolean isActivated;
    Integer cantidad;
    int cantidadPrimitiva;
    boolean isActivatedprimitivo;

    public ProductoDTO() {
    	
    }
    
 public ProductoDTO(Productos p) {
    	this.idProducto = p.getId();    	
    }
    
    
    public Integer getIdProducto() {
	return idProducto;
}

public void setIdProducto(Integer idProducto) {
	this.idProducto = idProducto;
}

	public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }

    public Boolean getIsActivated() {
        return isActivated;
    }

    public void setIsActivated(Boolean isActivated) {
        this.isActivated = isActivated;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidadPrimitiva() {
        return cantidadPrimitiva;
    }

    public void setCantidadPrimitiva(int cantidadPrimitiva) {
        this.cantidadPrimitiva = cantidadPrimitiva;
    }

    public boolean isActivatedprimitivo() {
        return isActivatedprimitivo;
    }

    public void setActivatedprimitivo(boolean isActivatedprimitivo) {
        this.isActivatedprimitivo = isActivatedprimitivo;
    }

}
