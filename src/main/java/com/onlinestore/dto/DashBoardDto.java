package com.onlinestore.dto;

import java.util.List;

public class DashBoardDto {
	
	private int countClients;
	private int totalFacturas;
	private int pendienteEnvio;
	
	private List<ProductosMasVendidosDto> listMasVendido;
	private List<TotalMesDto> listTotalMes;
	
	public List<ProductosMasVendidosDto> getListMasVendido() {
		return listMasVendido;
	}
	public void setListMasVendido(List<ProductosMasVendidosDto> listMasVendido) {
		this.listMasVendido = listMasVendido;
	}
	public List<TotalMesDto> getListTotalMes() {
		return listTotalMes;
	}
	public void setListTotalMes(List<TotalMesDto> listTotalMes) {
		this.listTotalMes = listTotalMes;
	}
	public int getCountClients() {
		return countClients;
	}
	public void setCountClients(int countClients) {
		this.countClients = countClients;
	}
	public int getTotalFacturas() {
		return totalFacturas;
	}
	public void setTotalFacturas(int totalFacturas) {
		this.totalFacturas = totalFacturas;
	}
	public int getPendienteEnvio() {
		return pendienteEnvio;
	}
	public void setPendienteEnvio(int pendienteEnvio) {
		this.pendienteEnvio = pendienteEnvio;
	}
	
	
	
}
