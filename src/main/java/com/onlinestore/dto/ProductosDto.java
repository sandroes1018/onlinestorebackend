package com.onlinestore.dto;

import java.io.Serializable;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.onlinestore.entity.Productos;

public class ProductosDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 92164030929895253L;

	/**
	 * 
	 */

	private int id;
	
	@NotBlank(message = "nombre is mandatory")
	private String nombre;
	@NotNull(message = "cantidad is mandatory")
	@Min(1)
	private int cantidad;
	@NotNull(message = "categoria is mandatory")
	private CategoriaDto categoria;
	@NotNull(message = "cantidad is mandatory")
	@Min(1)
	private double precio;
	@NotBlank(message = "urlImg is mandatory")
	private String urlImg;

	public ProductosDto() {
		this.categoria = new CategoriaDto();
	}

	public ProductosDto(Productos productos) {

		this.id = productos.getId();
		this.nombre = productos.getNombre();
		this.cantidad = productos.getCantidad();
		this.categoria = new CategoriaDto(productos.getIdcategoria());
		this.precio = productos.getPrecioproductoList().get(0).getPrecio();
		this.urlImg = productos.getUrlimg();

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public CategoriaDto getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaDto categoria) {
		this.categoria = categoria;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getUrlImg() {
		return urlImg;
	}

	public void setUrlImg(String urlImg) {
		this.urlImg = urlImg;
	}

}
