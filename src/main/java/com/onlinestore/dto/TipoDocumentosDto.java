package com.onlinestore.dto;

import java.io.Serializable;

public class TipoDocumentosDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7614307581284273887L;
	private int id;
	private String desc;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}	
	
}
