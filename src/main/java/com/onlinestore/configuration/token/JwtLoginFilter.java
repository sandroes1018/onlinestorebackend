/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.configuration.token;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.onlinestore.dto.UsuarioDto;
import com.onlinestore.utils.ServiUtils;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;


public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {

    public JwtLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException, ServletException {
        UsuarioDto usuarioDto = new ObjectMapper()
                .readValue(req.getInputStream(), UsuarioDto.class);
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        usuarioDto.getNombre(),
                        usuarioDto.getPassword(),
                        Collections.emptyList()
                )
        );
    }
    /**
     *
     * @param req
     * @param res
     * @param chain
     * @param auth
     * @throws IOException
     * @throws ServletException
     */
    @Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth
    ) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;

        response = ServiUtils.addAuthAngularResponse(response);

        final HttpServletRequest request = (HttpServletRequest) req;
        if (request.getMethod().equals("OPTIONS")) {
            chain.doFilter(req, res);
        }
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();

        response.setStatus(HttpServletResponse.SC_OK);
        //res.getWriter().write(jsonResp.toString());
        String JWT = AuthTokenService
                .addAuthentication(response, auth.getName());

        HashMap<String, Object> userLogin = new HashMap();

        userLogin.put("usuario", auth.getName());       
        userLogin.put("auth", JWT);
     

        String jsonRespString = ow.writeValueAsString(userLogin);
        response.getWriter().write(jsonRespString);
        response.getWriter().flush();
        response.getWriter().close();

    }


}
