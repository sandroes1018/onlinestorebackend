/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.configuration.token;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import com.onlinestore.utils.Constantes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

import static java.util.Collections.emptyList;
import org.jboss.logging.Logger;

class AuthTokenService {

    static String addAuthentication(HttpServletResponse res, String username) {
        try {
            String JWT = Jwts.builder()
                    .setSubject(username)
                    .setExpiration(new Date(
                            System.currentTimeMillis()
                            + Constantes.EXPIRATIONTIME)
                    )
                    .signWith(SignatureAlgorithm.HS512, Constantes.SECRET)
                    .compact();
            /**
             * agregar token aleader de response
             * res.addHeader(ConstantesServitv.HEADER_STRING,
             * ConstantesServitv.TOKEN_PREFIX + " " + JWT);
             */
            return JWT;
        } catch (Exception e) {
            System.out.println("error :" + e.getMessage());
        }
        return null;
    }

    static Authentication getAuthentication(HttpServletRequest req) throws Exception {

        try {
            String token = req.getHeader(Constantes.HEADER_STRING);
            String authHeader = ((HttpServletRequest) req)
                    .getHeader(org.springframework.http.HttpHeaders.AUTHORIZATION);
            if (token != null) {
                // parse the token.
                String user = Jwts.parser()
                        .setSigningKey(Constantes.SECRET)
                        .parseClaimsJws(authHeader.replace(Constantes.TOKEN_PREFIX, ""))
                        .getBody()
                        .getSubject();
                return user != null ? new UsernamePasswordAuthenticationToken(
                        user, null, emptyList()) : null;
            }
        } catch (Exception e) {
            if (e.getMessage().contains(Constantes.EXPIRED_TOKEN)) {
                throw new Exception(Constantes.MSN_SESSION_EXPIRADA);
            }
            Logger.getLogger("ERROR: ")
                    .log(Logger.Level.ERROR, e);
            throw new Exception(e.getMessage());
        }
        return null;
    }
}
