/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.configuration.token;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import com.onlinestore.utils.Constantes;
import com.onlinestore.utils.ServiUtils;

import org.springframework.security.core.Authentication;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;

public class JwtAuthFilter extends GenericFilterBean {

    /**
     *
     * @param req
     * @param res
     * @param filterChain
     * @throws IOException
     * @throws ServletException
     */
    @Override
    public void doFilter(
            ServletRequest req,
            ServletResponse res,
            FilterChain filterChain
    ) throws IOException, ServletException {

        final HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        try {
            response = ServiUtils.addAuthAngularResponse(response);
            if (request.getMethod().equals("OPTIONS")) {
                response.flushBuffer();
            }
            /* autenticacion token */
            Authentication authentication;
            authentication = AuthTokenService.getAuthentication(
                    (HttpServletRequest) request
            );
            /* exepcion usuario o contraseña incorrecto*/
            SecurityContextHolder.getContext().setAuthentication(authentication);
            if (response.getStatus() == 401) {
                throw new Exception("Error Usuario o Contraseña");
            }

            filterChain.doFilter(request, response);
        } catch (Exception ex) {
            /* si token session del usuario ha expirado retorna error*/
            if (ex.getMessage().contains(Constantes.MSN_SESSION_EXPIRADA)) {
                response.sendError(403, ex.getMessage());
            }
                Logger.getLogger(JwtAuthFilter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
