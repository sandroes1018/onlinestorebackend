/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.utils;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jlagosmu
 */
public class ServiUtils {

    public static HttpServletResponse addAuthAngularResponse(HttpServletResponse response) {

        response.setHeader("Access-Control-Allow-Origin",
                Constantes.URL_ORIGEN
        );
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods",
                "POST, PUT, GET, OPTIONS, DELETE"
        );
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
                "X-Requested-With, Authorization, Origin, Content-Type, Version");
        response.setHeader("Access-Control-Expose-Headers",
                "X-Requested-With, Authorization, Origin, Content-Type"
        );

        return response;
    }

    
    /**
     * valida si una lista o coleccion en null
     * @param list
     * @return
     */
    public static boolean isListNull(Collection list)
    {
        return !(list != null && !list.isEmpty());
    }

}
