/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.onlinestore.utils;


import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author jlagosmu
 */
public class MessageUtilReturn {

    /**
     *
     * @param clave
     * @param valor
     * @return
     */
    public static HashMap responseMessage(String clave, String valor) {
        HashMap<String, String> response = new HashMap();
        response.put(clave, valor);
        return response;
    }
    
}
